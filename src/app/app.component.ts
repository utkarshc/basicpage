import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent implements OnInit {
  sideNavStatus: boolean = false;
  color: string = '';
  color1: string = '';

  ngOnInit(): void {
    document.documentElement.style.setProperty('--ravi', 'red');
    document.documentElement.style.setProperty('--wb', 'yellow');
    this.font('sans-serif');
    console.log("color",this.color)
  }
  title = 'code';

  // f(type: string, className: string) {
  //   document.documentElement.style.setProperty(className, type);
  // }
  // font(t: Event | null) {
  //   console.log('check', t);
  //   if (!t) return;
  //   document.documentElement.style.setProperty(
  //     '--font',
  //     (t.target as  typePartial).value
  //   );
  // }
  font(t:string){
    document.documentElement.style.setProperty('--font', t)
  }
}
