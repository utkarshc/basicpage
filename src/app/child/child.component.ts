import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
})
export class ChildComponent implements OnInit {
  @Input() style: any;
  constructor() {}

  ngOnInit(): void {
    console.log('style', this.style);
  }
}
